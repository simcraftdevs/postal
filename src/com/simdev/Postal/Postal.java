package com.simdev.Postal;

import java.util.HashMap;
import java.util.LinkedList;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.Chest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;
import org.bukkit.plugin.java.*;

public class Postal extends JavaPlugin implements Listener {
	Player player;
	HashMap<String, MailBox> boxes = new HashMap<String, MailBox>(3);
	LinkedList<String> pendingBoxes = new LinkedList<String>();
	
	/* Enum to for switch statements */
	private enum CMDSTR {SEND, CLAIM, SETBOX, DELBOX, LIST, BADCMD, REGISTER};

	public void onEnable(){
		getServer().getPluginManager().registerEvents(this, this);
		// Read txt or dat file and populate messages that need to be claimed
	}
	public void onDisable(){
		// Write messages yet to be claimed in txt or dat file
	}

	@EventHandler
	public void onLogin(PlayerLoginEvent event){
		String playerName = event.getPlayer().getPlayerListName();
		
		if(boxes.containsKey(playerName)){ /* Player exists */
			boxes.get(playerName).setPlayer(event.getPlayer());
			return;	
		}
		else return;
			
	}
	public boolean onCommand(CommandSender sender, Command cmd, String CommandLabel, String[] args){
		player = (Player) sender;
		MailBox pmb = boxes.get(player.getPlayerListName());
		
		if (pmb == null && cmdParse(cmd.getName()) != CMDSTR.REGISTER){
			player.sendMessage("You are not in the system, please register with /register");
			return true;
		}	
		try {
			switch(cmdParse(cmd.getName())){
				case SEND:
					sendTest(player);
					return true;
				case CLAIM:
					if(args.length >= 1){
						pmb.claimLetter(Integer.parseInt(args[0]));
						return true;
					}
					else {
						pmb.listLetters();	
						return false;
					}
				case SETBOX:
					/* Notify user on mailbox overwrite */
					if(pmb.getBox() != null){
						player.sendMessage("Notice: You have already created a mailbox!");
						pendingBoxes.add(player.getPlayerListName());
						return true;
					} else {
						boxes.put(player.getPlayerListName(), new MailBox(player));
						pendingBoxes.add(player.getPlayerListName());
					}
					
					/* Make sure Java doesn't pull a fast one */
					if(boxes.get(player.getPlayerListName()) == null){
						player.sendMessage("Something really bad happened and we couldn't create your mailbox...\nSend us a bug report and we'll get working on it!");
						return true;
					}
				case DELBOX:
					return true;
				case LIST:
					pmb.listLetters();
					return true;
				case REGISTER:
					if(boxes.containsKey(player.getPlayerListName())){
						player.sendMessage("You are already in the mail system");
						return false;
					} else {
						boxes.put(player.getPlayerListName(), new MailBox(player));
						return true;
					}
				default: 
					player.sendMessage("Could not parse command. Argument was " + cmd.getName());
					return false;
			}
 
		} catch (Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	private void sendLetter(){
		// Get the inventory of the player's mailbox
		
		// Get an item that is a book
		// if no book error out
		
		// Remove the item from the players mail box
		// Remove all items from the player's mail box
		// Add the item to the recipient's mailbox
	}

	/* This function creates a test letter, adds an attachment and sents it to the correct player */
	private void sendTest(Player p){
		Letter l = new Letter();
		ItemStack items = new ItemStack(Material.COAL, 13);
		
		l.setTitle("First Letter!");
		l.setRecipient(player.getPlayerListName());
		l.setMessage("This is the very first letter to be claimed!");
		l.setAuthor("Tree");

		/* FIXME: Get Items from chest inventory - make sure the MailBox has a chest */
		l.addAttachments(items);
		
		// Find recieving mailbox
		MailBox pmb = boxes.get(this.getServer().getPlayer(l.getRecipient()).getPlayerListName());
		
		if(pmb == null)
		{
			// Create new mailbox
			p.sendMessage(l.getRecipient() + " does not have a mailbox setup. Creating one for them...");
			pmb = new MailBox(p);
			boxes.put(player.getPlayerListName(), pmb);
		}
		pmb.addLetter(l);
		p.sendMessage("Message successfully sent!");
	}
	
	private CMDSTR cmdParse(String cmd){
		if(cmd.equals("send"))
			return CMDSTR.SEND;
		
		if(cmd.equals("claim"))
			return CMDSTR.CLAIM;
		
		if(cmd.equals("listmail"))
			return CMDSTR.LIST;
		
		if(cmd.equals("setbox"))
			return CMDSTR.SETBOX;
		
		if(cmd.equals("delbox"))
			return CMDSTR.DELBOX;

		if(cmd.equals("register"))
			return CMDSTR.REGISTER;

		return CMDSTR.BADCMD;
	}

	@EventHandler
	public void onRightClick(PlayerInteractEvent event){
		BlockState b = null;
		Player p = event.getPlayer();
		MailBox box = boxes.get(p.getPlayerListName());
		/* FIXME: Skip chest animation */
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK && pendingBoxes.contains(p.getPlayerListName())){
			b = event.getClickedBlock().getState();

			/* FIXME: Joint chests i.e. chests connected to two players */
			if(b instanceof Chest){
				if(box == null){    /* The player is not in the mail system */
					p.sendMessage("Adding you to the mail system.");
					box = new MailBox(p, (Chest)b);
					boxes.put(p.getPlayerListName(), box);
					pendingBoxes.remove(p.getPlayerListName());
					return;
				} else {
					box.setChest((Chest)b);
					p.sendMessage("Successfully changed your outbox.");
					pendingBoxes.remove(player.getPlayerListName());
					return;
				}				
			} else {
				p.sendMessage("That is not a chest.");
				return;
			}
		} else return;
	}

	// private void ReadDat();
	// private void SaveDat();
	
	// private void ReadContentsOnly
}


