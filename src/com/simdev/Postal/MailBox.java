package com.simdev.Postal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;

import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

// Represents a player's mailbox inventory
public class MailBox {
	
	/* All unclaimed letters */
	private HashMap<Integer, Letter> letters;
	
	/* Number of messages in mailbox */
	private int size;
	
	/* Owner of the mailbox */
	private String owner;

	/* Player reference of owner - changes on each login */
	private Player player = null;
	
	/* Chest reference that will be the outgoing items */
	private Chest box = null;

	public MailBox(Player p){
		player = p;
		owner = p.getPlayerListName();
		letters = new HashMap<Integer, Letter>(3);
		// loadLetters();
	}
	
	public MailBox(){
		letters = null;
		owner = null;
	}

	public String getOwner(){
		return owner;
	}
	
	public HashMap<Integer, Letter> getLetters(){
		return letters;
	}
	
	public void addLetter(Letter letter){
		letters.put(size++, letter);
		return;
	}

	/* WARNING: Player reference check */	
	public void claimLetter(int id){
		Letter l = null;
		if(letters.get(id) == null){
			player.sendMessage("That letter doesn't exist or you don't have a mailbox set up.");
		} else {
			l = letters.get(id);
			this.readLetter(id);
			
			for(ItemStack is : l.getAttachments()){
				HashMap<Integer, ItemStack> res = player.getInventory().addItem(is);
				
				if(!res.isEmpty()){
					player.sendMessage("Inventory is too full, can't claim all attachments");
					l.addAttachments(res.get(0));
				}
			}
			letters.remove(id);
			player.sendMessage("Items successfully added to inventory!");
		}
	}
	
	public boolean isEmpty(){
		return letters.isEmpty();
	}
	
	/* WARNING: Player reference check */
	public void readLetter(int id){
		Letter l = null;
		
		/* Don't try to open a letter if it doesn't exist */
		if(letters.get(id) != null){
			player.sendMessage(letters.get(id).getMessage());
			
			/* Delete the letter after reading if it doens't have any attachments */
			if(!letters.get(id).hasAttachments())
				letters.remove(id);
		} else {
			player.sendMessage("That message doesn't exist! Make sure you have the right letter id.");
		}
	}
	
	/* WARNING: Player reference check */
	public void listLetters(){
		if(isEmpty()){
			player.sendMessage("Your mailbox is empty!");
		} else {
			for(int i = 0; i < size; i++){
				if(letters.get(i) == null)
					continue;
				else {
					player.sendMessage(i + " - " + letters.get(i).getTitle());
				}
			}
		}
	}
	
	public boolean equals(MailBox box){
		if(box == null){
			return this == null;
		}
		return box.owner.equals(owner);
	}

	public void setPlayer(Player p){
		player = p;
	}

	public void setChest(Chest c){
		box = c;
	}

	public Chest getBox(){
		return box;
	}
}
