package com.simdev.Postal;

// This looks like garbage! 

import java.util.ArrayList;
import java.util.Map;
import java.io.Serializable;
import java.io.*;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

// Have it be a book but our own kind of book
public class Letter implements Serializable {
	
	// A reference to the author Player
	String author;
	
	// A reference to the recipient
	String recipient;
	
	// The message to be sent
	String message;
	
	// Title of the letter
	String title;
	
	// Attachments with the letter
	// Default serialization should be skipped; readObject() and writeObject() will take care of it
	transient ArrayList<ItemStack> attachments = new ArrayList<ItemStack>();
	
	
	public String getAuthor(){
		return author; 
	}
	
	public ArrayList<ItemStack> getAttachments(){
		return attachments;
	}
	
	public String getMessage(){
		return message;
	}
	
	public String getRecipient(){
		return recipient;
	}
	
	public String getTitle(){
		return title;
	}
	
	
	/* Setters */
	public void setAuthor(String a){
		author = a;
	}

	public void setAttachments(ArrayList<ItemStack> items){
		for(ItemStack is : items){
			attachments.add(is);
		}
		
		return;
	}
	

	public void setMessage(String mes){
		message = mes;
	}
	
	public void setRecipient(String rec){
		recipient = rec;
	}
	
	public void setTitle(String letterTitle){
		title = letterTitle;
	}

		
	/* Misc Operations */
	public void addAttachments(ItemStack is){
		if(is != null)
			attachments.add(is);
		else
			throw new IllegalArgumentException();
		
		return;
	}
	
	public void clearAttachments(){
		attachments.clear();
		return;
	}
	
	public boolean hasAttachments(){
		return attachments.size() != 0;
	}

	/* Used to store contents in a hashtable */
	public int hashCode(){
		final int salt = 31;
		
		return salt * author.hashCode() * recipient.hashCode() * title.hashCode();
	}

	public boolean equals(Letter l){
		if (l == null){
			return this == null;
		}
		return hashCode() == l.hashCode();
	}

	private void writeObject(ObjectOutputStream oos)
	throws IOException {
		// default serialization
		oos.defaultWriteObject();
		ArrayList<Map<String, Object>> attachs = new ArrayList<Map<String, Object>>();

		// write ItemStacks out
		for(ItemStack is : attachments)
		{
			attachs.add(is.serialize());
		}

		oos.writeObject(attachs);
	}

	private void readObject(ObjectInputStream ois)
	throws ClassNotFoundException, IOException {
		// Default deserialization
		ois.defaultReadObject();

		ArrayList<Map<String, Object>> attachs = (ArrayList<Map<String, Object>>)ois.readObject();
		attachments = new ArrayList<ItemStack>();

		for(Map<String,Object> at : attachs){
			attachments.add(ItemStack.deserialize(at));
		}
	}
}
